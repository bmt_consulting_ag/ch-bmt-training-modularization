sap.ui.define([
    "sap/m/MessageBox",
    "ch/bmt/training/modularization/myLib/Formatter"],
    function (MessageBox, Formatter) {
        'use strict';

        return {
            /**
             * 
             * @param {*} sMsg 
             * @param {*} sTitle 
             */
            reportSuccess: function (sMsg, sTitle) {
                debugger;
                MessageBox.show(
                    Formatter.capitalizeFirstLetter(sMsg), {
                    title: Formatter.capitalizeFirstLetter(sTitle)
                });
            }
        };

    });