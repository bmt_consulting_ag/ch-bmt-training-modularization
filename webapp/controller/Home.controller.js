sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "ch/bmt/training/modularization/myLib/MessageManager"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageManager) {
        "use strict";

        return Controller.extend("ch.bmt.training.modularization.controller.Home", {
            onInit: function () {

            },

            /**
             * 
             * @param {*} oEvent 
             */
            onButtonPressed: function (oEvent) {
                MessageManager.reportSuccess("der Button wurde gedrückt.", "erfolg");
            }
        });
    });
